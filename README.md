## How to start the server

- clone this repo `git clone https://github.com/cfghyd22/team-57.git code_for_good`
- cd into the repo `cd code_for_good`
- go to development branch `git checkout development`
- cd into the server `cd server`
- install dependencies `npm install`
- go to the shared drive folder and download the file named .env
- paste that file in server folder make sure it is name .env not env
- run the project using `npm run dev`
- go to the given link in terminal

## Git workflow

- Go to development branch from VS-code and do: `git pull origin development`
- Branch out from development branch using: `git checkout -b branch-name`
- Make your changes in your branch locally.
- Once you are done making the changes: 
`git add .`
`git commit -m "commit-message"`
`git pull origin development`
`git push origin -u branch-name`
Merge feature branch with development branch on github website.