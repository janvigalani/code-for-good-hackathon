#! /bin/env node
//@ts-check

require('dotenv').config()
const User = require('../models/User')
const Course = require('../models/Course')
const Meeting = require('../models/Meeting')
const Lecture = require('../models/Lecture')
const Completed = require('../models/Completed')
const mongoose = require('mongoose')
const configService = require('../config/configService')

async function main() {
  await mongoose.connect(configService.getConfig('MONGO_URI'))
  console.log('mongodb connected')
  // await User.collection.drop()
  await Course.collection.drop()
  await Lecture.collection.drop()
  await Completed.collection.drop()
  // await Meeting.collection.drop()

  await mongoose.connection.close()
}

main().catch((err) => console.log(err))
