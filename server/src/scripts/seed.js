#! /bin/env node
//@ts-check
require('dotenv').config()
const User = require('../models/User')
const Course = require('../models/Course')
const Completed = require('../models/Completed')
const mongoose = require('mongoose')
const configService = require('../config/configService')
const bcryptjs = require('bcryptjs')
const Lecture = require('../models/Lecture')

const googleImageLink =
  'https://media.istockphoto.com/photos/student-in-multiple-choice-test-picture-id1317503980'

// const youtubeVideoLink = 'https://i.ytimg.com/vi_webp/z3Nw5o9dS7Q/0.webp'
const youtubeVideoLink =
  'https://www.youtube.com/embed/z3Nw5o9dS7Q?autoplay=1&mute=1'

const videoCode = 'z3Nw5o9dS7Q'

async function createUser({ name, region, superLady }) {
  return User.create({
    name: name,
    email: name + '@gmail.com',
    phoneNumber: '0123456789',
    password: hashPassword('password'),
    isEmailVerified: true,
    isPhoneVerified: true,
    authority: superLady._id,
    region: region,
  })
}

async function createSuperLady({ name, admin, region }) {
  return User.create({
    name: name,
    email: name + '@gmail.com',
    phoneNumber: '0123456789',
    password: hashPassword('password'),
    isEmailVerified: true,
    isPhoneVerified: true,
    authority: admin._id,
    role: 'superlady',
    region: region,
  })
}

async function createAdmin() {
  return User.create({
    name: 'admin',
    email: 'admin@gmail.com',
    phoneNumber: '0123456789',
    password: hashPassword('password'),
    isEmailVerified: true,
    isPhoneVerified: true,
    role: 'admin',
    region: 'maharashtra',
  })
}
async function createCourse({ name, language, category }) {
  return Course.create({
    name,
    language,
    duration: '25 hour',
    category,
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores, incidunt nesciunt assumenda voluptatem nihil temporibus quis fuga consequuntur animi, nisi, hic nemo aperiam cum praesentium iure non corrupti deleniti eius?',
    imageUrl: googleImageLink,
  })
}

async function createLecture({ name, course }) {
  return Lecture.create({
    name,
    course: course._id,
    videoUrl: youtubeVideoLink,
    videoCode: videoCode,
  })
}

function hashPassword(password) {
  const salt = bcryptjs.genSaltSync(10)
  const hash = bcryptjs.hashSync(password, salt)
  return hash
}

async function main() {
  await mongoose.connect(configService.getConfig('MONGO_URI'))
  console.log('mongodb connected')
  /*
   */
  const admin = await createAdmin()

  const sup1 = await createSuperLady({
    name: 'superlady1',
    admin,
    region: 'maharashtra',
  })
  const sup2 = await createSuperLady({
    name: 'superlady2',
    admin,
    region: 'gujarat',
  })

  const user1 = await createUser({
    name: 'user1',
    region: 'maharashtra',
    superLady: sup1,
  })
  const user2 = await createUser({
    name: 'user2',
    region: 'maharashtra',
    superLady: sup1,
  })
  const user3 = await createUser({
    name: 'user3',
    region: 'maharashtra',
    superLady: sup1,
  })
  const user4 = await createUser({
    name: 'user4',
    region: 'gujarat',
    superLady: sup2,
  })
  const user5 = await createUser({
    name: 'user5',
    region: 'gujarat',
    superLady: sup2,
  })
  const user6 = await createUser({
    name: 'user6',
    region: 'gujarat',
    superLady: sup2,
  })
  const users = await User.find({})
  console.log(users.map((u) => u.toObject()))
  /*
  const course1 = await createCourse({
    name: 'course1',
    language: 'english',
    category: 'programming',
  })
  const course2 = await createCourse({
    name: 'course2',
    language: 'hindi',
    category: 'programming',
  })
  const course3 = await createCourse({
    name: 'course3',
    language: 'english',
    category: 'cooking',
  })

  const lecture1 = await createLecture({
    name: 'lecture1',
    course: course1,
  })
  const lecture2 = await createLecture({
    name: 'lecture2',
    course: course1._id,
  })
  const lecture3 = await createLecture({
    name: 'lecture3',
    course: course2._id,
  })
  const lecture4 = await createLecture({
    name: 'lecture4',
    course: course2._id,
  })

  user1.courses.push(course1._id)
  user1.courses.push(course2._id)

  user2.courses.push(course1._id)
  user2.courses.push(course2._id)
  user2.courses.push(course3._id)

  const completed1 = await Completed.create({
    user: user1._id,
    lecture: lecture1._id,
  })
  const completed2 = await Completed.create({
    user: user1._id,
    lecture: lecture3._id,
  })

  await user1.save()
  await user2.save()
  */

  await mongoose.connection.close()
}

main().catch(console.log)
