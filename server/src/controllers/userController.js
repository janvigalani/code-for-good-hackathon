const express = require('express')
const { populateLocals, catchAsync } = require('../utils')
const authenticate = require('../middleware/authenticate')
const Course = require('../models/Course')
const User = require('../models/User')

module.exports = function (app) {
  const router = express.Router()

  router.post('/course/enroll', async (req, res) => {
    const { courseId } = req.body
    const user = await User.findById(req.session.userId)
    const course = await Course.findById(courseId)
    if (user.courses.includes(courseId)) {
      req.flash('error', 'You are already enrolled in this course')
      res.redirect(`/courses/${courseId}`)
      return
    }
    user.courses.push(course._id)
    await user.save()
    return res.redirect(`/courses/${courseId}`)
  })

  router.get(
    '/home',
    authenticate(),
    catchAsync(async (req, res) => {
      await req.user.populate('courses')
      console.log(req.user)
      populateLocals(req, res)
      res.render('user_home', {
        courses: req.user.courses,
      })
    })
  ),
    app.use('/user', router)
}
