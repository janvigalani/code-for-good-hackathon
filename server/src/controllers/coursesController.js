const express = require('express')
const authenticate = require('../middleware/authenticate')
const Course = require('../models/Course')
const Lecture = require('../models/Lecture')
const User = require('../models/User')
const { populateLocals } = require('../utils')

module.exports = function (app) {
  const router = express.Router()

  router.get('/', authenticate(), async (req, res) => {
    const language = req.query.language
    const category = req.query.category
    let courses = await Course.find()
    if (language) {
      courses = courses.filter((course) => course.language === language)
    }
    if (category) {
      courses = courses.filter((course) => course.category === category)
    }
    populateLocals(req, res)
    res.render('courses', {
      courses: courses,
    })
  })

  router.get('/:id', authenticate(), async (req, res) => {
    const user = await User.findById(req.session.userId)
    const course = await Course.findById(req.params.id)
    const lectures = await Lecture.find({ course: course._id })
    let isEnrolled = false
    if (user.courses.includes(course._id)) {
      isEnrolled = true
    }
    populateLocals(req, res)
    res.render('courses_id', {
      lectures,
      course,
      isEnrolled,
    })
  })

  app.use('/courses', router)
}
