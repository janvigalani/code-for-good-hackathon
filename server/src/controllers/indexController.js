//@ts-check
const express = require('express')
const authenticate = require('../middleware/authenticate')
const preventAuth = require('../middleware/blockAuthenticated')
const User = require('../models/User')
const { isLoggedIn, populateLocals, extractUser } = require('../utils')

module.exports = function (app) {
  const router = express.Router()
  router.get('/', async (req, res) => {
    //@ts-ignore
    populateLocals(req, res)
    if (!isLoggedIn(req)) {
      res.redirect('/home')
      return
    } else {
      //@ts-ignore
      console.log(req.session.userId)
      const user = await User.findById(req.session.userId)
      if (user.role == 'user') {
        res.redirect('/user/home')
      } else if (user.role == 'admin') {
        res.redirect('/admin/home')
      } else if (user.role == 'superlady') {
        res.redirect('/superlady/home')
      } else {
        res.redirect('/home')
      }
      return
    }
  })
  router.get('/login', preventAuth(), (req, res) => {
    populateLocals(req, res)
    res.render('login')
  })
  router.get('/signup', preventAuth(), (req, res) => {
    populateLocals(req, res)
    res.render('signup')
  })
  router.get('/home', async (req, res) => {
    const user = await User.findById(req.session.userId)
    if (user) {
      req.user = user
    }
    populateLocals(req, res)
    res.render('home')
  })
  router.get('/profile', authenticate(), (req, res) => {
    populateLocals(req, res)
    res.render('profile')
  })
  router.get(
    '/admin',
    authenticate({
      allowedRoles: ['admin'],
    }),
    (req, res) => {
      populateLocals(req, res)
      res.render('admin')
    }
  )
  app.use('/', router)
}
