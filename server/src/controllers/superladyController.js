const express = require('express')
const { Schema, model } = require('mongoose')
const { populateLocals } = require('../utils')
const authenticate = require('../middleware/authenticate')
const User = require('../models/User');
const Course = model("Course");
const Meeting = require('../models/Meeting');
const bcryptjs = require('bcryptjs')
const { catchAsync, extractErrorMessage } = require('../utils')

module.exports = function (app) {
  const router = express.Router()

  router.get(
    '/home',
    authenticate({
      allowedRoles: ['superlady'],
    }),
    (req, res) => {
      populateLocals(req, res)
      res.render('superlady_home')
    }
  )

  // register user form
  router.get(
    '/user/register',
    authenticate({ allowedRoles: ['superlady'] }),
    (req, res) => {
      populateLocals(req, res)
      res.render('superlady_user_register')
    }
  )

  router.post(
    '/user/register',
    authenticate({ allowedRoles: ['superlady'] }),
    catchAsync(async (req, res) => {
      populateLocals(req, res)
      const authority = req.session.userId

      const { name, password, email, phone_number, region } = req.body

      const existingByEmail = await User.findOne({ email: email })

      if (existingByEmail) {
        req.flash('errorMessages', 'email already exists')
        res.redirect('/signup')
        return
      }

      const salt = bcryptjs.genSaltSync(10)
      const hash = bcryptjs.hashSync(password, salt)
      const user = await User.create({
        name,
        password: hash,
        email,
        phoneNumber: phone_number,
        role: 'user',
        region,
        authority,
      })
      req.flash('successMessages', 'User added successfully!')
      res.redirect('/superlady/user/register')
      return
    })
  )

  router.get(
    '/user/enroll',
    authenticate({ allowedRoles: ['superlady'] }),
    catchAsync(async (req, res) => {
      populateLocals(req, res)

      const user = await User.findById(req.session.userId)
      console.log(user)
      const usersRegistered = await User.find({ authority: user._id })
      console.log(usersRegistered)
      // const authority = user.authority

      // const users = await User.find({
      //   authority,
      // })

      let requiredUsersData = []
      usersRegistered.forEach((user) => {
        requiredUsersData.push({
          id: user.id,
          name: user.name,
        })
      })

      const courses = await Course.find()
      let requiredCourses = []

      courses.forEach((course) => {
        requiredCourses.push({
          id: course.id,
          name: course.name,
        })
      })

      res.render('superlady_user_enroll', {
        requiredUsersData,
        requiredCourses,
      })
    })
  )

  router.post(
    '/user/enroll',
    authenticate({ allowedRoles: ['superlady'] }),
    catchAsync(async (req, res) => {
      populateLocals(req, res)
      const courseId = req.body.course
      const userId = req.body.user
      const user = await User.findById(userId)
      user.courses.push(courseId)
      user.save()
      console.log(user)
      res.redirect('/superlady/user/enroll')
    })
  )

  router.get('/meetings/add',
    authenticate({allowedRoles : ['superlady']}),
    catchAsync( async (req, res) => {
      populateLocals(req, res);
      res.render('superlady_meeting_add');
    })
  )
  

  router.post('/meetings/add',
    authenticate({allowedRoles : ['superlady']}),
    catchAsync( async (req, res) => {
      populateLocals(req, res);
      const {title, date, time, location, region, description} = req.body;
      const superlady = req.session.userId;
      const meeting = new Meeting(
        {
          title, 
          date, 
          time, 
          location, 
          region, 
          description,
          superlady
        }
      )
      await meeting.save();
      req.flash(
        'successMessages',
        'Meeting added successfully!'
      )
      res.redirect('/meetings');
    })
  )

  app.use('/superlady', router)
}
