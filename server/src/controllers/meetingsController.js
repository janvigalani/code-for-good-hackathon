const express = require('express')
const authenticate = require('../middleware/authenticate')
const { populateLocals } = require('../utils')
const Meeting = require('../models/Meeting');
const User = require('../models/User');

module.exports = function (app) {
  const router = express.Router()

  router.get('/', authenticate(),async (req, res) => {
    populateLocals(req, res)
    const meetings = await Meeting.find();
    let requiredMeetings = [];
    for(let i = 0; i<meetings.length; i++) {
      const superladyId = meetings[i].superlady;
      const user = await User.findById(superladyId);
      let meeting = {
        title: meetings[i].title,
        date: meetings[i].date,
        time: meetings[i].time,
        location: meetings[i].location,
        region: meetings[i].region,
        superlady: user.name,
        description: meetings[i].description
      }
      requiredMeetings.push(meeting);
      // console.log(meetings[i]);
    };
    res.render('meetings', {requiredMeetings});
  })

  router.get('/:id', authenticate(), (req, res) => {
    populateLocals(req, res)
    res.render('meetings_id')
  })

  app.use('/meetings', router)
}
