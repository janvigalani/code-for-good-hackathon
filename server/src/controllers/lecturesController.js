const express = require('express')
const authenticate = require('../middleware/authenticate')
const Lecture = require('../models/Lecture')
const { populateLocals } = require('../utils')

module.exports = function (app) {
  const router = express.Router()

  router.get('/', authenticate(), (req, res) => {
    populateLocals(req, res)
    res.render('lectures')
  })

  router.get('/:id', authenticate(), async (req, res) => {
    const lecture = await Lecture.findById(req.params.id)
    console.log(lecture)
    populateLocals(req, res)
    res.render('lectures_id', {
      lecture,
    })
  })

  app.use('/lectures', router)
}
