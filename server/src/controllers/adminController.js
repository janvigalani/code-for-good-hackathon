const express = require('express')
const { populateLocals, catchAsync } = require('../utils')
const authenticate = require('../middleware/authenticate')
const User = require('../models/User')
const bcryptjs = require('bcryptjs')
const { request } = require('express')
const Course = require('../models/Course')
const Lecture = require('../models/Lecture')

module.exports = function (app) {
  const router = express.Router()

  router.get(
    '/home',
    authenticate({
      allowedRoles: ['admin'],
    }),
    (req, res) => {
      populateLocals(req, res)
      res.render('admin_home')
    }
  )

  router.get(
    '/superlady/register',
    authenticate({
      allowedRoles: ['admin'],
    }),
    (req, res) => {
      populateLocals(req, res)
      res.render('admin_superlady_register')
    }
  )

  router.post(
    '/superlady/register',
    // authenticate({allowedRoles : ['superlady']}),
    catchAsync(async (req, res) => {
      let userId = req.session.userId
      let user = await User.findById(userId)
      populateLocals(req, res)
      const authority = user.authority
      const { name, password, email, phone_number, region } = req.body

      const existingByEmail = await User.findOne({ email: email })

      if (existingByEmail) {
        req.flash('errorMessages', 'email already exists')
        res.redirect('/superlady/register')
        return
      }

      const salt = bcryptjs.genSaltSync(10)
      const hash = bcryptjs.hashSync(password, salt)
      const savedUser = await User.create({
        name,
        password: hash,
        email,
        phoneNumber: phone_number,
        role: 'superlady',
        region,
        authority,
      })
      req.flash(
        'successMessages',
        'Signup successful, confirm your email to login'
      )
      res.redirect('/admin/superlady/register')
      return
    })
  )

  //create course page
  router.get(
    '/courses/add',
    authenticate({
      allowedRoles: ['admin'],
    }),
    async (req, res) => {
      /*
    TODO: get user id from session and get the user from db using id
    let current_loggedin_user = await User.findOne({ _id: req.session.userId });
    */
      let userId = req.session.userId
      let user = await User.findById(userId)

      if (user.role != 'admin') {
        return res.redirect('./login')
      }
      populateLocals(req, res)
      res.render('admin_add_course')
    }
  )

  // store course
  // /*
  router.post(
    '/courses/add',
    catchAsync(async (req, res) => {
      let userId = req.session.userId
      let user = await User.findById(userId)

      if (user.role != 'admin') {
        return res.redirect('..')
      }

      populateLocals(req, res)
      const { name, language, category, description, duration, imageUrl } =
        req.body

      const course = await Course.create({
        name,
        language,
        category,
        description,
        duration,
        imageUrl,
      })

      req.flash('successMessages', 'Course created successfully!!!')
      return res.redirect('/admin/courses/add')
    })
  )

  router.get(
    '/courses/lectures/add',
    authenticate(),
    catchAsync(async (req, res) => {
      let userId = req.session.userId
      let user = await User.findById(userId)

      if (user.role != 'admin') {
        return res.redirect('./login')
      }

      const courses = await Course.find()
      let requiredCourses = []

      courses.forEach((course) => {
        requiredCourses.push({
          id: course.id,
          name: course.name,
        })
      })

      populateLocals(req, res)
      res.render('admin_courses_lectures_add', { requiredCourses })
    })
  )

  router.post(
    '/courses/lectures/add',
    authenticate(),
    catchAsync(async (req, res) => {
      const { course, name, videoCode } = req.body
      const videoUrl = `https://www.youtube.com/embed/${videoCode}?autoplay=1&mute=1`
      const lecture = new Lecture({
        course,
        videoUrl,
        videoCode,
        name,
      })
      await lecture.save()
      populateLocals(req, res)
      req.flash('successMessages', 'Lecture added successfully!!!')
      res.redirect('/admin/courses/lectures/add')
    })
  )

  app.use('/admin', router)
}
