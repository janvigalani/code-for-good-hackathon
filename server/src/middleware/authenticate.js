//@ts-check
const User = require('../models/User')
const { extractUser, isLoggedIn } = require('../utils')

function normalizeOpts(opts) {
  if (!opts) {
    opts = {}
  }
  if (!opts.allowedRoles) {
    opts.allowedRoles = ['user', 'admin', 'superlady']
  }
  return opts
}

function isRoleAllowed(role, allowedRoles) {
  let found = false
  for (let allowedRole of allowedRoles) {
    if (role == allowedRole) {
      found = true
      break
    }
  }
  return found
}

module.exports = function (opts) {
  return function (req, res, next) {
    opts = normalizeOpts(opts)
    //check if logged in
    if (!isLoggedIn(req)) {
      req.flash('errorMessages', 'please login to continue')
      res.redirect('/login')
      return
    }
    User.findById(req.session.userId, (err, user) => {
      if (err) {
        console.log(err)
        req.flash('errorMessages', 'please login to continue')
        res.redirect('/login')
        return
      } else {
        //check authorization
        if (!isRoleAllowed(user.role, opts.allowedRoles)) {
          req.flash('errorMessages', 'not authorized')
          res.redirect('/')
          return
        }
        req.user = user
        next()
      }
    })
  }
}
