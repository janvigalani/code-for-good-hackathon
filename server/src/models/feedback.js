const mongoose = require('mongoose')
const Schema = mongoose.Schema

const feedbackSchema = mongoose.Schema({
  course: { type: Schema.Types.ObjectId, ref: 'Course', required: true },
  user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  text: { type: Schema.Types.String, required: true },
  rating: { type: Schema.Types.Number, required: true },
})
module.exports = mongoose.model('Feedback', feedbackSchema)
