const mongoose = require('mongoose')

const lectureSchema = mongoose.Schema({
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course',
    required: true,
  },
  videoUrl: { type: mongoose.Schema.Types.String, required: true },
  name: { type: mongoose.Schema.Types.String, required: true },
  videoCode: { type: mongoose.Schema.Types.String },
})

module.exports = mongoose.model('Lecture', lectureSchema)
