//@ts-check
const { Schema, model } = require('mongoose')

const MeetingSchema = new Schema({
    title: {
        type: Schema.Types.String,
        required: true
    },
    date: {
        type: Schema.Types.String,
        required: true
    },
    time: {
        type: Schema.Types.String,
        required: true
    },
    location: {
        type: Schema.Types.String,
        required: true
    },
    region: {
        type: Schema.Types.String,
        required: true
    },
    superlady: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    description: {
        type: Schema.Types.String,
        required: true
    }
})

const Meeting = model('Meeting', MeetingSchema)

module.exports = Meeting
