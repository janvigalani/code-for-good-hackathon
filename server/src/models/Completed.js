const mongoose = require('mongoose')

const completedSchema = mongoose.Schema({
  lecture: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Lecture',
  },
  user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
})
module.exports = mongoose.model('Completed', completedSchema)
