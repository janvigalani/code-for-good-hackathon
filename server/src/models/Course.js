//@ts-check
const { Schema, model } = require('mongoose')

const CourseSchema = new Schema({
    name: {
        type: Schema.Types.String,
        required: true
    },
    language: {
        type: Schema.Types.String,
        required: true
    },
    category: {
        type: Schema.Types.String,
        required: true
    },
    description: {
        type: Schema.Types.String,
        required: true
    },
    duration: {
        type: Schema.Types.String,
        required: true
    },
    imageUrl: {
        type: Schema.Types.String,
        required: true
    }
})

const Course = model('Course', CourseSchema)

module.exports = Course
